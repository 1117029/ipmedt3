   window.onload = function() {
    const scene = document.getElementById('js--scene')
    const places = document.getElementsByClassName('js--place');
    const camera = document.getElementById("js--camera");
    const tutorial = document.getElementById("js--tutorial");
    const doorgaan = document.getElementById("js--doorgaan");
    const alarm = document.getElementById("js--brandalarm");
    const brandlamp = document.getElementById("js--brandlamp");
    const telefoon = document.getElementById("js--telefoon");
    const brandblusser = document.getElementById("js--brandblusser");
    const printer = document.getElementById("js--printer");
    const brandblusserHand = document.getElementById("js--brandblusserHand");
    const tijd = document.getElementById("js--timer");
    const vuur = document.getElementById("js--vuur");
    const fout = document.getElementById("js--foutmelding");
    var volgorde = 1;

    const timer = () => {
        let minuten = 0;
        let seconden = 1;
        this.setInterval(() => {
            tijd.setAttribute("position", "-0.1 0.75 -1")
            tijd.setAttribute("text", "value", `${minuten} : ${seconden}`)
            if (seconden == 59) {
                seconden = 0;
                minuten += 1;
            } else {
                seconden++;
            }
        }, 1000);
        // wanneer er 5 minuten voorbij zijn eindscherm laten zien
        // feedbackscherm
    }

    function sleep(ms) {
        return new Promise(resolve => setTimeout(resolve, ms));
    }

    alarmSound = new Audio('./sound/slow-whoop.mp3');
    alarmSound.addEventListener('ended', function() {
        this.currentTime = 0;
        this.play();
    }, false);

    callSound = new Audio('./sound/call.mp3');
    callSound.addEventListener('ended', function() {
        this.currentTime = 0;
        this.play();
    }, false);

    for (let i = 0; i < places.length; i++) {
        places[i].addEventListener('click', function(evt) {
            let speed = 2000;
            let att = document.createAttribute("animation");
            let x = this.getAttribute("position").x;
            let z = this.getAttribute("position").z;
            att.value = "property: position; easing: linear; dur: " + speed + "; to: " + x + " 1.6 " + z;
            camera.setAttribute("animation", att.value);
        });

    }

    //tutorial skippen
    doorgaan.addEventListener('click', async function(evt) {
        if(volgorde == 1){
          //kunnen bellen nadat je de tutorial hebt geskipt
          tutorial.remove();
          timer();
          startSmoke();
          volgorde += 1;
        }
        else{
          camera.innerHTML += '<a-entity gltf-model="#foutmelding-glb" id="fout" class="clickable" rotation="0 -90 0" position="-0.25 -0.5 -3" scale="0.3 0.3 0.3"></a-entity>';
          const fout = document.getElementById("fout");
          await sleep(3000);
          fout.remove();
        }
      })

    telefoon.addEventListener('click', async function(evt) {
        if(volgorde == 2){
          camera.innerHTML += '<a-entity gltf-model="#telefoon-glb" id="test" class="clickable" rotation="0 -90 0" position="0 -0.5 -1.5" scale="0.3 0.3 0.3"></a-entity>';
          const tel = document.getElementById("test");
          telefoon.remove();
          callSound.play();
          await sleep(7000);
          callSound.pause();
          textToSpeech("Er is brand op mijn locatie, help!");
          await sleep(3500);
          tel.remove();
          volgorde += 1;
        }
        else{
          camera.innerHTML += '<a-entity gltf-model="#foutmelding-glb" id="fout" class="clickable" rotation="0 -90 0" position="-0.25 -0.5 -3" scale="0.3 0.3 0.3"></a-entity>';
          const fout = document.getElementById("fout");
          await sleep(3000);
          fout.remove();
        }
    });

    // brandalarm af laten gaan
    alarm.addEventListener('click', async function(evt) {
      if(volgorde == 3){
        console.log('speel geluid af (slow-whoop)');
        alarmSound.play();
        let att = document.createAttribute('animation');
        att.value = "property: light.intensity; from: 0; to: 2; dur: 1000; loop: true; easing: linear; dir: reverse;";
        brandlamp.setAttribute('animation', att.value);
        volgorde += 1;
      }
      else{
        camera.innerHTML += '<a-entity gltf-model="#foutmelding-glb" id="fout" class="clickable" rotation="0 -90 0" position="-0.25 -0.5 -3" scale="0.3 0.3 0.3"></a-entity>';
        const fout = document.getElementById("fout");
        await sleep(3000);
        fout.remove();
      }
    });

    // brandblusser oppakken
    brandblusser.addEventListener('click', async function(evt) {
      if(volgorde == 4){
        camera.innerHTML += '<a-entity gltf-model="#brandblusserHand-glb" id="js--brandblusser" class="clickable" rotation="0 -75 0" position="0.5 -1.5 -1" scale="0.2 0.2 0.2"></a-entity>';
        this.remove();
        printer.addEventListener('click', function(evt) {
          vuur.remove();
          alarmSound.pause();
        });
        vuur.addEventListener('click', function(evt) {
          vuur.remove();
          alarmSound.pause();
        });
      }
      else{
        camera.innerHTML += '<a-entity gltf-model="#foutmelding-glb" id="fout" class="clickable" rotation="0 -90 0" position="-0.25 -0.5 -3" scale="0.3 0.3 0.3"></a-entity>';
        const fout = document.getElementById("fout");
        await sleep(3000);
        fout.remove();
      }
    });

    function textToSpeech(text){
      fetch("https://api.voicerss.org/?key=da7c0c32a89642a598ddbe1c68b00a82&hl=nl-nl&src=" + text + "")
                .then(response => {
	                 console.log(response);
                   var audio = document.createElement('audio');
                   audio.src = response.url;
                   audio.play();
                 })
                 .catch(err => {
	                  console.log(err);
                  });
    }

    async function startSmoke(){
      var seconds = 300;
      var elapsed = 0;
      var fogAmount = 50;
      while (elapsed !== seconds){
        fogAmount = fogAmount - 0.14333;
        elapsed = elapsed + 1;
        console.log(fogAmount);
        scene.setAttribute('fog', "type: linear; color: #6e6e78; far:" + fogAmount +  "; near: 0")
        await sleep(1000);
      }


    }

}
